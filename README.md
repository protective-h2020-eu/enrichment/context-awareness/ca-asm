PROTECTIVE  Context Awareness (CA) - Asset State
========== 

This component is designed to determine vulnerability level of asset (based on software/software list given as an input). This tool is called by CA MAIR and should be launched on a host reachable from the CA MAIR host.

Prerequisites
-------------

Prerequisites are that the machine (or a VM) has Docker installed. The testing environment was based on Ubuntu 18.04 and Docker 18.06.1.

Install
-------

To install the products:
- download the as.tar.xz archive
- unpack the archive.
```
tar -xf as.tar.xz
```
- load docker image.
``` 
sudo docker load<as.tar
```
Run
---

To run AS:

```
sudo docker run -p 5000:5000 as
```
If you want to test:

```
curl -i -H "Content-Type: application/json" -d'{"product": "Internet Explorer","vendor":"Microsoft Corporation","version":"11.0.9600.18792","id":"IE1"}' http://127.0.0.1:5000/api/get-software-max-cvss
```


API Reference
---

Read the API Reference [here](./api-description-v1.03.pdf). 